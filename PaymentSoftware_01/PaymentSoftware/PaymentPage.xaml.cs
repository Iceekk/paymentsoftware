﻿using DatabaseModels;
using MahApps.Metro.Controls.Dialogs;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PaymentSoftware
{
    /// <summary>
    /// Interaction logic for PaymentPage.xaml
    /// </summary>
    public partial class PaymentPage: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Person person;
        public Person Person
        {
            get
            {
                return this.person;
            }
            set
            {
                person = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Person"));
            }
        }

        public PaymentPage()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public PaymentPage(string egn)
        {
            InitializeComponent();
            this.DataContext = this;

            this.FillForm(egn);
        }

        private void FillForm(string egn)
        {
            Person personFound = new PersonService().FindPerson(egn);

            if (personFound == null)
            {
                MessageBox.Show($"Грешка! Няма такъв човек с посеченото ЕГН!");
                return;
            }

            this.person = personFound;

            this.Firstname.Text = person.Firstname;
            this.Middlename.Text = person.Middlename;
            this.Lastname.Text = person.Lastname;
            this.Egn.Text = person.Egn;
            this.Address.Text = person.Address;

            DataGrid.Columns.Clear();
            DataGrid.Items.Clear();
            this.GenerateObligationsTable(this.person.Obligations.ToList());
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            this.Close();
            window.ShowDialog();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            var egn = this.SearchField.Text;

            if(egn == null)
            {
                MessageBox.Show($"Моля, въведете правилно ЕГН!");
                return;
            }
            if(egn.Length == 0)
            {
                MessageBox.Show($"Моля, въведете ЕГН!");
                return;
            }

            Person personFound = new PersonService().FindPerson(egn);

            if (personFound == null)
            {
                MessageBox.Show($"Грешка! Няма такъв човек с посеченото ЕГН!");
                return;
            }

            this.person = personFound;

            this.Firstname.Text = person.Firstname;
            this.Middlename.Text = person.Middlename;
            this.Lastname.Text = person.Lastname;
            this.Egn.Text = person.Egn;
            this.Address.Text = person.Address;

            DataGrid.Columns.Clear();
            DataGrid.Items.Clear();
            this.GenerateObligationsTable(this.person.Obligations.ToList());

        }

        private void GenerateObligationsTable(List<Obligation> obligations)
        {
            DataGridTextColumn c1 = new DataGridTextColumn();
            c1.Header = "№";
            c1.Binding = new Binding("ObligationId");
            c1.Width = 45;
            DataGrid.Columns.Add(c1);

            DataGridTextColumn c2 = new DataGridTextColumn();
            c2.Header = "Ресурс";
            c2.Binding = new Binding("ObligationType");
            c2.Width = 95;
            DataGrid.Columns.Add(c2);

            DataGridTextColumn c3 = new DataGridTextColumn();
            c3.Header = "Сума";
            c3.Binding = new Binding("Amount");
            c3.Width = 55;
            DataGrid.Columns.Add(c3);

            DataGridTextColumn c4 = new DataGridTextColumn();
            c4.Header = "Дата";
            c4.Binding = new Binding("ObligationDate");
            c4.Width = 95;
            DataGrid.Columns.Add(c4);

            var buttonTemplate = new FrameworkElementFactory(typeof(Button));
            buttonTemplate.SetBinding(Button.ContentProperty, new Binding("Pay"));
            buttonTemplate.AddHandler(
                Button.ClickEvent,
                new RoutedEventHandler(async(o, e) => {

                    PersonObligationViewModel pickedPerson = (PersonObligationViewModel)DataGrid.SelectedItem;

                    var mySettings = new MetroDialogSettings()
                    {
                        AffirmativeButtonText = "Назад",
                        NegativeButtonText = "Изпринти",
                        AnimateShow = true,
                        AnimateHide = false
                    };
                    var result = await this.ShowMessageAsync(
                        "Вие успешно платихте!",
                        "Желаете ли да генерирате квитанция?",
                        MessageDialogStyle.AffirmativeAndNegative, mySettings);

                    PersonService personService = new PersonService();
                    personService.DeleteObligation(pickedPerson.ObligationId);
                    this.FillForm(this.SearchField.Text);
                    var closeMe = result == MessageDialogResult.Affirmative;

                    if (!closeMe) new RecordToPDF().ExportObligationToPdf(pickedPerson);
                    
                })
            );

            DataGrid.Columns.Add(
                new DataGridTemplateColumn()
                {
                    Header = "-",
                    Width = 80,
                    CellTemplate = new DataTemplate() { VisualTree = buttonTemplate }
                }
            );
            
            //List<PersonObligationViewModel> personObligations = new List<PersonObligationViewModel>();

            foreach (Obligation obligation in obligations)
            {
                string obl = "";
                if (obligation.ObligationType == 0)
                {
                    obl = "Ел. енергия";
                }
                else if (obligation.ObligationType == 1)
                {
                    obl = "Студена вода";
                }
                else if (obligation.ObligationType == 2)
                {
                    obl = "Топла вода";
                }

                DataGrid.Items.Add(new PersonObligationViewModel(obligation.ObligationId, obligation.Amount, obligation.ObligationDate,
                    obl, "Плати"));
                
            }

        }
    }
}
