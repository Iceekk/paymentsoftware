﻿using iText.IO.Font.Constants;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using MahApps.Metro.Controls.Dialogs;
using Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PaymentSoftware
{
    /// <summary>
    /// Interaction logic for RecordsPage.xaml
    /// </summary>
    public partial class RecordsPage
    {
        private List<string> records = new List<string>() { "Оставащи задължения", "Постъпили суми за последния 1 месец", "Постъпили суми за последните 3 месеца", "Постъпили суми за последната 1 година" };
        public List<string> Records { get { return records; } set => records = value; }
        //public string selectedRecord = "";
        //public String SelectedRecord { get { return selectedRecord; } set{ selectedRecord = Record.SelectedValue.ToString(); } }



        public RecordsPage()
        {
            InitializeComponent();
            this.DataContext = this;

            //LabelName.Content = Record.SelectedValue.ToString();
        }

        private void GenerateTable()
        {
            DataGridTextColumn c1 = new DataGridTextColumn();
            c1.Header = "Задължение";
            c1.Binding = new Binding("ObligationType");
            c1.Width = 105;
            DataGrid.Columns.Add(c1);

            DataGridTextColumn c2 = new DataGridTextColumn();
            c2.Header = "Обща сума";
            c2.Binding = new Binding("Amount");
            c2.Width = 105;
            DataGrid.Columns.Add(c2);

            DataGridTextColumn c3 = new DataGridTextColumn();
            c3.Header = "Длъжници";
            c3.Binding = new Binding("Users");
            c3.Width = 105;
            DataGrid.Columns.Add(c3);

            List<ObligationViewModel> obligations = new RecordService().RemainingObligationsRecord();
            double sum = 0;

            foreach (ObligationViewModel obligation in obligations)
            {
                DataGrid.Items.Add(new ObligationViewModel(obligation.ObligationType, obligation.Amount, obligation.Users));
                sum += obligation.Amount;
            }

            this.All.Content = "Обща дължима сума: " + sum + " лева";

        }

        private void GenerateTableWithRecords(int period)
        {
            DataGridTextColumn c1 = new DataGridTextColumn();
            c1.Header = "Ресурс";
            c1.Binding = new Binding("ObligationType");
            c1.Width = 105;
            DataGrid.Columns.Add(c1);

            DataGridTextColumn c2 = new DataGridTextColumn();
            c2.Header = "Обща сума";
            c2.Binding = new Binding("Amount");
            c2.Width = 105;
            DataGrid.Columns.Add(c2);

            DataGridTextColumn c3 = new DataGridTextColumn();
            c3.Header = "Брой Платили";
            c3.Binding = new Binding("Users");
            c3.Width = 105;
            DataGrid.Columns.Add(c3);

            List<PaymentViewModel> payments = new RecordService().AmountsReceivedForPeriod(period);
            double sum = 0;

            foreach (PaymentViewModel payment in payments)
            {
                DataGrid.Items.Add(new PaymentViewModel(payment.ObligationType, payment.Amount, payment.Users));
                sum += payment.Amount;
            }

            this.All.Content = "Обща постъпила сума за периода: " + sum + " лева";

        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            this.Close();
            window.ShowDialog();
        }

        private void Clean_Click(object sender, RoutedEventArgs e)
        {
            this.All.Content = "";
            this.Record.Text = "Моля, изберете справка";
            DataGrid.Columns.Clear();
            DataGrid.Items.Clear();
            DataGrid.DataContext = null;
        }

        private void Print_Click(object sender, RoutedEventArgs e)
        {

            RecordToPDF r = new RecordToPDF();

            List<PaymentViewModel> payments = new RecordService().AmountsReceivedForPeriod(1);


            var mySettings = new MetroDialogSettings()
            {
                AffirmativeButtonText = "Назад",
                NegativeButtonText = "Изпринти",
                AnimateShow = true,
                AnimateHide = false
            };
            
            var result =  this.ShowMessageAsync(
                    "Внимание!",
                    "Желаете ли да генерирате .pdf файл?",
                    MessageDialogStyle.AffirmativeAndNegative, mySettings);

            //var closeMe = (int)MessageDialogResult.Affirmative;

            r.ExportRecordToPdf(payments);

        }

        private void Record_Click(object sender, RoutedEventArgs e)
        {
            var choice = Record.SelectedValue.ToString();

            if(choice == "Оставащи задължения")
            {
                DataGrid.Columns.Clear();
                DataGrid.Items.Clear();
                this.GenerateTable();
            }
            else if (choice == "Постъпили суми за последния 1 месец")
            {
                DataGrid.Columns.Clear();
                DataGrid.Items.Clear();
                this.GenerateTableWithRecords(1);
            }
            else if (choice == "Постъпили суми за последните 3 месеца")
            {
                DataGrid.Columns.Clear();
                DataGrid.Items.Clear();
                this.GenerateTableWithRecords(3);
            }
            else if (choice == "Постъпили суми за последната 1 година")
            {
                DataGrid.Columns.Clear();
                DataGrid.Items.Clear();
                this.GenerateTableWithRecords(12);
            }

        }

    }
}
