﻿using DatabaseModels;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PaymentSoftware
{
    /// <summary>
    /// Interaction logic for PeoplePage.xaml
    /// </summary>
    public partial class PeoplePage
    {
        public PeoplePage()
        {
            InitializeComponent();
            GenerateTable();
        }

        private void GenerateTable()
        {
            //DataGrid dataGrid = new DataGrid();

            DataGridTextColumn c1 = new DataGridTextColumn();
            c1.Header = "№";
            c1.Binding = new Binding("PersonId");
            c1.Width = 60;
            PeopleDataGrid.Columns.Add(c1);

            DataGridTextColumn c2 = new DataGridTextColumn();
            c2.Header = "Име";
            c2.Binding = new Binding("Firstname");
            c2.Width = 105;
            PeopleDataGrid.Columns.Add(c2);

            DataGridTextColumn c3 = new DataGridTextColumn();
            c3.Header = "Презиме";
            c3.Binding = new Binding("Middlename");
            c3.Width = 110;
            PeopleDataGrid.Columns.Add(c3);

            DataGridTextColumn c4 = new DataGridTextColumn();
            c4.Header = "Фамилия";
            c4.Binding = new Binding("Lastname");
            c4.Width = 110;
            PeopleDataGrid.Columns.Add(c4);

            DataGridTextColumn c5 = new DataGridTextColumn();
            c5.Header = "ЕГН";
            c5.Binding = new Binding("Egn");
            c5.Width = 110;
            PeopleDataGrid.Columns.Add(c5);

            DataGridTextColumn c6 = new DataGridTextColumn();
            c6.Header = "Адрес";
            c6.Binding = new Binding("Address");
            c6.Width = 150;
            PeopleDataGrid.Columns.Add(c6);

            DataGridTextColumn c7 = new DataGridTextColumn();
            c7.Header = "Задължения";
            c7.Binding = new Binding("ObligationCount");
            c7.Width = 110;
            PeopleDataGrid.Columns.Add(c7);

            var buttonTemplate = new FrameworkElementFactory(typeof(Button));
            buttonTemplate.SetBinding(Button.ContentProperty, new Binding("ButtonInfo"));
            buttonTemplate.AddHandler(
                Button.ClickEvent,
                new RoutedEventHandler((o, e) => {

                    PersonViewModel pickedPerson = (PersonViewModel)PeopleDataGrid.SelectedItem;

                    PaymentPage paymentPage = new PaymentPage(pickedPerson.Egn);
                    this.Close();
                    paymentPage.ShowDialog();
                })
            );
            
            PeopleDataGrid.Columns.Add(
                new DataGridTemplateColumn()
                {
                    Header = "Информация",
                    Width = 110,
                    CellTemplate = new DataTemplate() { VisualTree = buttonTemplate }
                }
            );


            List<Person> people = new PeopleService().getPeople();

            foreach(Person person in people)
            {
                PeopleDataGrid.Items.Add(new PersonViewModel(person.PersonId, person.Firstname, person.Middlename, person.Lastname, person.Egn, 
                    person.Address, person.Obligations.Count(), "--->"));
                
            }
            
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            this.Close();
            window.ShowDialog();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            var egn = this.SearchField.Text;

            if (egn == null)
            {
                MessageBox.Show($"Моля, въведете правилно ЕГН!");
                return;
            }
            if (egn.Length == 0)
            {
                MessageBox.Show($"Моля, въведете ЕГН!");
                return;
            }

            Person personFound = new PersonService().FindPerson(egn);

            if (personFound == null)
            {
                MessageBox.Show($"Грешка! Няма такъв човек с посеченото ЕГН!");
                return;
            }

            PeopleDataGrid.Items.Clear();
            PeopleDataGrid.Items.Add(new PersonViewModel(personFound.PersonId, personFound.Firstname, personFound.Middlename, personFound.Lastname,
                personFound.Egn, personFound.Address, personFound.Obligations.Count(), "--->"));

        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            PeopleDataGrid.Columns.Clear();
            PeopleDataGrid.Items.Clear();
            PeopleDataGrid.DataContext = null;
            this.GenerateTable();
        }
    }
}
