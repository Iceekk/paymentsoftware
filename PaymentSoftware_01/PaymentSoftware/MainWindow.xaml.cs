﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PaymentSoftware
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void User_Btn_Click(object sender, RoutedEventArgs e)
        {
            // go to People window
            PeoplePage peoplePage = new PeoplePage();
            this.Close();
            peoplePage.ShowDialog();
        }

        private void Payment_Btn_Click(object sender, RoutedEventArgs e)
        {
            // go to Payment window
            PaymentPage paymentPage = new PaymentPage();
            App.Current.MainWindow.Close();
            paymentPage.ShowDialog();
        }

        private void Records_Btn_Click(object sender, RoutedEventArgs e)
        {
            // go to Records window
            RecordsPage recordsPage = new RecordsPage();
            App.Current.MainWindow.Close();
            recordsPage.ShowDialog();
        }
    }
}
