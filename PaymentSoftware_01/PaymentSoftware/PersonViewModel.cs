﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentSoftware
{
    public class PersonViewModel
    {
        public PersonViewModel()
        {

        }

        public PersonViewModel(int PersonId,
         string Firstname,
         string Middlename,
         string Lastname,
         string Egn,
         string Address,
         int ObligationCount,
         string ButtonInfo
         )
        {
            this.PersonId = PersonId;
            this.Firstname = Firstname;
            this.Middlename = Middlename;
            this.Lastname = Lastname;
            this.Egn = Egn;
            this.Address = Address;
            this.ObligationCount = ObligationCount;
            this.ButtonInfo = ButtonInfo;
        }

        public int PersonId { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Egn { get; set; }
        public string Address { get; set; }
        public int ObligationCount { get; set; }
        public string ButtonInfo { get; set; }

    }
}
