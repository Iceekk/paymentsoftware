﻿public enum ObligationType
{
    ELECTRICITY = 0,
    HOT_WATER = 1,
    COLD_WATER = 2,
}
