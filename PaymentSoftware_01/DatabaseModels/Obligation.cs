﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseModels
{
    public class Obligation
    {
        public int ObligationId { get; set; }
        public double Amount { get; set; }
        public System.DateTime ObligationDate { get; set; }
        public int ObligationType { get; set; }
        public Nullable<int> Person_PersonId { get; set; }

        public virtual Person Person { get; set; }
    }
}
