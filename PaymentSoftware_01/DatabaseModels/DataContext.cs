﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseModels
{
    public class DataContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Obligation> Obligations { get; set; }
        public DbSet<Payment> Payments { get; set; }

        public DataContext() : base(Properties.Settings.Default.DbConnect)
        {
            //this.Configuration.LazyLoadingEnabled = false;
        }
    }
}
