﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseModels
{
    public class Payment
    {
        public Payment(double Amount, DateTime ObligationDate, DateTime PaymentDate, ObligationType ObligationType, string PersonId)
        {
            this.Amount = Amount;
            this.ObligationDate = ObligationDate;
            this.PaymentDate = PaymentDate;
            this.ObligationType = (int)ObligationType;
            this.PersonId = PersonId;
        }

        public int PaymentId { get; set; }
        public double Amount { get; set; }
        public System.DateTime ObligationDate { get; set; }
        public System.DateTime PaymentDate { get; set; }
        public int ObligationType { get; set; }
        public string PersonId { get; set; }
    }
}
