﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseModels
{
    public partial class Person
    {
       
        public Person()
        {
            this.Obligations = new HashSet<Obligation>();
        }

        public int PersonId { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Egn { get; set; }
        public string Address { get; set; }
        
        public virtual ICollection<Obligation> Obligations { get; set; }
    }
}
