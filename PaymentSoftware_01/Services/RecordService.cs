﻿using DatabaseModels;
using PaymentSoftware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class RecordService
    {
        readonly DataContext dataContext = new DataContext();

        public List<ObligationViewModel> RemainingObligationsRecord()
        {
            List<ObligationViewModel> record = new List<ObligationViewModel>();

            var obligations = (from obl in dataContext.Obligations
                     group obl by new { obl.ObligationType } into hh
                     select new
                     {
                         hh.Key.ObligationType,
                         Amount = hh.Sum(s => s.Amount),
                         Users = hh.Count()
                     }).OrderByDescending(i => i.Amount).ToList();



            foreach(var obligation in obligations)
            {
                string obl = "";
                if (obligation.ObligationType == 0)
                {
                    obl = "Ел. енергия";
                }
                else if (obligation.ObligationType == 1)
                {
                    obl = "Студена вода";
                }
                else if (obligation.ObligationType == 2)
                {
                    obl = "Топла вода";
                }

                record.Add(new ObligationViewModel(obl, obligation.Amount, obligation.Users));
            }

            //Console.WriteLine(q[1].ObligationType + " " + q[1].Amount + " " + q[1].Users);
            return record;
        }

        public List<PaymentViewModel> AmountsReceivedForPeriod(int period)
        {
            List<PaymentViewModel> records = new List<PaymentViewModel>();
            var periodDate = DateTime.Today.AddMonths(0 - period);

            var payments = (from pay in dataContext.Payments
                            where(pay.PaymentDate <= DateTime.Now && pay.PaymentDate >= periodDate)
                               group pay by new { pay.ObligationType } into hh
                               select new
                               {
                                   hh.Key.ObligationType,
                                   Amount = hh.Sum(s => s.Amount),
                                   Users = hh.Count()
                               }).OrderByDescending(i => i.Amount).ToList();

            foreach (var pay in payments)
            {
                string payment = "";
                if (pay.ObligationType == 0)
                {
                    payment = "Ел. енергия";
                }
                else if (pay.ObligationType == 1)
                {
                    payment = "Студена вода";
                }
                else if (pay.ObligationType == 2)
                {
                    payment = "Топла вода";
                }
                records.Add(new PaymentViewModel(payment, pay.Amount, pay.Users));
            }

            return records;
        }

    }
}
