﻿
using iTextSharp.text;
using iTextSharp.text.pdf;
using PaymentSoftware;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class RecordToPDF
    {
        
        public void ExportRecordToPdf(List<PaymentViewModel> payments)
        {
            try
            {
                Document pdfDoc = new Document(PageSize.LETTER, 40f, 40f, 60f, 60f);
                string path = $"C:\\Users\\Ico\\source\\repos\\PaymentSoftware_01\\Services\\Record.pdf";
                PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.OpenOrCreate));
                pdfDoc.Open();
               
                pdfDoc.Add(new Paragraph("Reference"));
                pdfDoc.Add(new Paragraph("Type: Remaing payments"));
                
                
                var columnWidths = new[] { 2f, 2f, 2f};
                var table = new PdfPTable(columnWidths)
                {
                    HorizontalAlignment = 1,
                    WidthPercentage = 100,
                    DefaultCell = { MinimumHeight = 22f }
                };

                var cell = new PdfPCell(new Phrase("More information: "))
                {
                    Colspan = 3,
                    HorizontalAlignment = 1,  //0=Left, 1=Centre, 2=Right
                    MinimumHeight = 30f
                };

                table.AddCell(cell);

                table.AddCell("Obligation type");
                table.AddCell("Money");
                table.AddCell("Number of users");

                int count = 1;
                payments
                    .ForEach(r =>
                    {
                        table.AddCell(count.ToString());
                        table.AddCell(r.Amount.ToString());
                        table.AddCell(r.Users.ToString());
                        count += 1;
                    });

                pdfDoc.Add(table);
                pdfDoc.Add(new Paragraph("Date: " + DateTime.Now));

                pdfDoc.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void ExportObligationToPdf(PersonObligationViewModel obligation)
        {
            try
            {
                Document pdfDoc = new Document(PageSize.LETTER, 40f, 40f, 60f, 60f);
                string path = $"C:\\Users\\Ico\\source\\repos\\PaymentSoftware_01\\Services\\Obligation.pdf";
                PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.OpenOrCreate));
                pdfDoc.Open();

                pdfDoc.Add(new Paragraph("Quittance"));
                //pdfDoc.Add(new Paragraph("Type: Remaing payments"));


                var columnWidths = new[] { 2f, 2f, 2f };
                var table = new PdfPTable(columnWidths)
                {
                    HorizontalAlignment = 1,
                    WidthPercentage = 100,
                    DefaultCell = { MinimumHeight = 22f }
                };

                var cell = new PdfPCell(new Phrase("More information: "))
                {
                    Colspan = 3,
                    HorizontalAlignment = 1,  //0=Left, 1=Centre, 2=Right
                    MinimumHeight = 30f
                };

                table.AddCell(cell);

                table.AddCell("Obligation type");
                table.AddCell("Money");
                table.AddCell("Number of users");
    
                table.AddCell("Electriciy".ToString());
                table.AddCell(obligation.Amount.ToString());
                table.AddCell(obligation.ObligationDate.ToString());
                       
                pdfDoc.Add(table);
                pdfDoc.Add(new Paragraph("Date: " + DateTime.Now));

                pdfDoc.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        
    }

}
