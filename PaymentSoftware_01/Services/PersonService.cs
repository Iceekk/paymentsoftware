﻿using DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class PersonService
    {
        readonly DataContext dataContext = new DataContext();

        public Person FindPerson(string egn)
        {
            return this.dataContext.People.FirstOrDefault(acc => acc.Egn == egn);
        }

        public void DeleteObligation(int obligationId)
        {
            Obligation obligationFound = this.dataContext.Obligations.FirstOrDefault(acc => acc.ObligationId == obligationId);

            this.dataContext.Obligations.Remove(obligationFound);
            this.dataContext.SaveChanges();
        }

        public void AddRecordToDB(double amount, DateTime obligationDate, DateTime payDate, ObligationType type, string egn)
        {
            Payment payment = new Payment(amount, obligationDate, payDate, type, egn);

            this.dataContext.Payments.Add(payment);
            this.dataContext.SaveChanges();
        }

    }
}
