﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentSoftware
{
    public class ObligationViewModel
    {
        public ObligationViewModel(string ObligationType, double Amount, int Users )
        {
            this.ObligationType = ObligationType;
            this.Amount = Amount;
            this.Users = Users;
        }

        public string ObligationType { get; set; }
        public double Amount { get; set; }
        public int Users { get; set; }
    }
}
