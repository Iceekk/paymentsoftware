﻿using DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class PeopleService
    {
        readonly DataContext dataContext = new DataContext();

        public List<Person> getPeople()
        {
            return this.dataContext.People.ToList();
        }

    }
}
