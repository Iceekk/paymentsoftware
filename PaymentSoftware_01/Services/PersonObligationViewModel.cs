﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class PersonObligationViewModel
    {
        public PersonObligationViewModel()
        {

        }

        public PersonObligationViewModel(int ObligationId, double amount, DateTime date, string type, string pay)
        {
            this.ObligationId = ObligationId;
            this.Amount = amount;
            this.ObligationDate = date;
            this.ObligationType = type;
            this.Pay = pay;
        }

        public int ObligationId { get; set; }
        public double Amount { get; set; }
        public DateTime ObligationDate { get; set; }
        public string ObligationType { get; set; }
        public string Pay { get; set; }
    }
}
